package ru.fnight.telegrambot;

import org.json.JSONObject;

public class Callback {

    public static final String TYPE_ACTION = "action";
    public static final String TYPE_SEARCH = "search";
    public static final String TYPE_ADD_NEW_SERIAL = "new_serial";
    public static final String TYPE_ADD_NEW_SERIAL_INLINE = "new_serial_inline";
    public static final String TYPE_SELECT_SERIAL = "select";
    public static final String TYPE_MAIN_MENU = "menu";
    public static final String TYPE_SELECT_NEW_SERIAL = "select_new_serial";
    public static final String TYPE_SHOW_SERIAL_CATEGORIES = "serial_categories";

    public static final String ACTION_DELETE = "x";
    public static final String ACTION_CHANGE_COUNT = "+-";
    public static final String ACTION_NEXT_SEASON = ">";
    public static final String ACTION_PREVIOUS_SEASON = "<";
    public static final String ACTION_CHECK_WATCHED = "v";

    private String id;
    private String type;
    private String action;
    private String data;

    Callback(String id, String type, String action, String data) {
        this.id = id;
        this.type = type;
        this.action = action;
        this.data = data;
    }

    Callback(String jString) {
        JSONObject jsonObject = new JSONObject(jString);
        this.id = jsonObject.has("id") ? jsonObject.getString("id") : null;
        this.type = jsonObject.has("type") ? jsonObject.getString("type") : null;
        this.action = jsonObject.has("action") ? jsonObject.getString("action") : null;
        this.data = jsonObject.has("data") ? jsonObject.getString("data") : null;
    }

    public String toJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", this.id);
        jsonObject.put("type", this.type);
        jsonObject.put("action", this.action);
        jsonObject.put("data", this.data);
        return jsonObject.toString();
    }

    public static Callback jsonToCallback(String jString) {
        JSONObject jsonObject = new JSONObject(jString);
        return new Callback(
                jsonObject.getString("id"),
                jsonObject.getString("type"),
                jsonObject.getString("action"),
                jsonObject.getString("data")
        );
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
