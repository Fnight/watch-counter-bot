package ru.fnight.telegrambot;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import static ru.fnight.telegrambot.Utils.readStream;

public class Serial extends Media {
    private static Integer STATUS_UNKNOWN = 0;
    private static Integer STATUS_ANNOUNCED = 1;
    private static Integer STATUS_ONGOING = 2;
    private static Integer STATUS_COMPLETE = 3;
    private static Integer STATUS_CLOSE = 4;

    private static Integer SEARCH_RESULTS_COUNT = 5;

    private List<Integer> episodes;

    public Serial() {
    }

    private Serial(String id, String title, String description, Date data, Integer status, List<Integer> episodes, Integer time) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = data;
        this.status = status;
        this.episodes = episodes;
        this.time = time;
    }

    static Serial searchSerial(HashMap<String, Serial> serials, String filmsToken, final String id) {
        Serial serial = null;
        Long startTime = System.currentTimeMillis();
        if (serials.containsKey(id)) {
            serial = serials.get(id);
        } else {
            try {
                String url = String.format("https://api.themoviedb.org/3/tv/%s?api_key=%s&language=ru", id, filmsToken);
                URL obj = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
                connection.setRequestMethod("GET");
                Integer responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String response = readStream(connection.getInputStream());
                    serial = parseSerial(new JSONObject(response));
                    serials.put(serial.getId(), serial);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        System.out.println(String.format("Get serial for: %s", System.currentTimeMillis() - startTime));
        return serial;
    }

    static List<Serial> searchSerials(HashMap<String, Serial> serials, String filmsToken, String serialName) {
        List<Serial> serialsFound = new ArrayList<>();
        try {
            String url = String.format("https://api.themoviedb.org/3/search/tv?api_key=%s&language=ru&query=%s",
                    filmsToken, URLEncoder.encode(serialName, "UTF-8"));
            URL obj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
            connection.setRequestMethod("GET");
            Integer responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String response = readStream(connection.getInputStream());
                JSONObject jObj = new JSONObject(response);
                JSONArray items = jObj.getJSONArray("results");
                for (int i = 0; i < (items.length() > SEARCH_RESULTS_COUNT ? SEARCH_RESULTS_COUNT : items.length()); i++) {
                    Integer id = items.getJSONObject(i).getInt("id");
                    serialsFound.add(searchSerial(serials, filmsToken, id.toString()));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return serialsFound.subList(0, serialsFound.size() > SEARCH_RESULTS_COUNT ? SEARCH_RESULTS_COUNT : serialsFound.size());
    }

    private static Serial parseSerial(JSONObject jsonObject) {
        Serial serial = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String title = jsonObject.getString("name");
            Long id = jsonObject.getLong("id");
            String description = jsonObject.getString("overview");
            List<Integer> episodes = new ArrayList<>();
            JSONArray seasons = jsonObject.getJSONArray("seasons");
            for (int i = 0; i < seasons.length(); i++) {
                episodes.add(seasons.getJSONObject(i).getInt("episode_count"));
            }
            Integer time = 0;
            if (jsonObject.has("episode_run_time") && jsonObject.getJSONArray("episode_run_time").length() > 0) {
                time = jsonObject.getJSONArray("episode_run_time").getInt(0);
            }
            String stringDate = "";
            if (jsonObject.has("first_air_date") && !jsonObject.isNull("first_air_data")) {
                stringDate = jsonObject.getString("first_air_date");
            }
            Date date;
            if (stringDate.equals("")) {
                date = null;
            } else {
                date = format.parse(jsonObject.getString("first_air_date"));
            }
            Integer status;
            String stringStatus = jsonObject.getString("status");
            switch (stringStatus) {
                case "Planned":
                    status = STATUS_ANNOUNCED;
                    break;
                case "Returning Series":
                    status = STATUS_ONGOING;
                    break;
                case "In Production":
                    status = STATUS_ONGOING;
                    break;
                case "Ended":
                    status = STATUS_COMPLETE;
                    break;
                case "Canceled":
                    status = STATUS_CLOSE;
                    break;
                case "Pilot":
                    status = STATUS_ANNOUNCED;
                    break;
                default:
                    status = STATUS_UNKNOWN;
                    break;
            }
            serial = new Serial(id.toString(), title, description, date, status, episodes, time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serial;
    }

    public Integer currentSeason(Integer countEpisodes) {
        Integer currentSeason = 0;
        Integer currentEpisodes = countEpisodes;
        while (this.getEpisodes().size() > currentSeason && currentEpisodes - this.getEpisodes().get(currentSeason) >= 0) {
            currentEpisodes -= this.getEpisodes().get(currentSeason);
            currentSeason++;
        }
        return currentSeason;
    }

    public Integer episodesForSeasonFromStart(Integer season) {
        Integer episodes = 0;
        for (int i = 0; i < season; i++) {
            episodes += this.getEpisodes().get(i);
        }
        return episodes;
    }

    public List<Integer> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<Integer> episodes) {
        this.episodes = episodes;
    }
}
