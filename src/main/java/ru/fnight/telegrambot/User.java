package ru.fnight.telegrambot;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

public class User {
    public static final Integer STEP_START = 0;
    public static final Integer STEP_WAIT_NAME_SERIAL = 1;

    private Long chatId;
    private List<MediaInformation> mediaInformations;
    private Integer step;

    public User(){
    }

    User(Long chatId) {
        this.chatId = chatId;
        this.mediaInformations = new ArrayList<>();
        this.step = 0;
    }

    static User searchUser(DatabaseReference database, final Long chatId) {
        final User[] users = new User[1];
        final Semaphore semaphore = new Semaphore(0);
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    users[0] = dataSnapshot.child("users").child(chatId.toString()).getValue(User.class);
                    if (users[0] != null && users[0].getMediaInformations() == null) {
                        users[0].setMediaInformations(new ArrayList<>());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    semaphore.release();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                semaphore.release();
            }
        });
        try {
            semaphore.acquire();
            return users[0];
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public MediaInformation searchMediaInformation(String id) {
        List<MediaInformation> result = this.mediaInformations.stream()
                .filter(media -> media.getId().equals(id))
                .collect(Collectors.toList());
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public List<MediaInformation> getMediaInformations() {
        return mediaInformations;
    }

    public void setMediaInformations(List<MediaInformation> mediaInformations) {
        this.mediaInformations = mediaInformations;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }
}
