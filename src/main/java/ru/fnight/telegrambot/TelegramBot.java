package ru.fnight.telegrambot;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.File;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.stream.IntStream;


import static ru.fnight.telegrambot.Serial.searchSerial;
import static ru.fnight.telegrambot.Serial.searchSerials;
import static ru.fnight.telegrambot.User.searchUser;


public class TelegramBot extends TelegramLongPollingBot {

    private DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    private final String ADD_NEW_SERIAL = "Добавить новый сериал";
    private final String ADD_MORE_SERIAL = "Добавить еще сериал";
    private final String LIST_SERIALS = "Список сериалов";
    private final String LIST_ACTIVE_SERIALS = "Список активных сериалов";
    private final String NO_ACTIVE_SERIALS = "Активных сериалов нет";
    private final String SETTINGS = "Настройки";
    private final String MAIN_MENU = "\uD83C\uDFE0";
    private final String MAIN = "Главное";
    private final String MENU = "Меню";
    private final String GETTING_SEARCH_GROUP = "Попробуем найти сериал в базе?\nВведите ответом на это сообщение запрос для поиска";
    private final String GETTING_SEARCH = "Попробуем найти сериал в базе?\nВведите запрос для поиска";
    private final String RESULTS = "Результаты:\n";
    private final String NOT_FOUND_SEARCH = "Такой сериал не найден.\nПопробуйте найти его в источнике: https://www.themoviedb.org/\nИ повторите запрос";
    private final String USER_NOT_FOUND = "Вы не найдены в базе, пройдите авторизацию через /start";
    private final String PREVIOUS_SEASON = "⏪";
    private final String NEXT_SEASON = "⏩";
    private final String DELETE_SERIAL = "\uD83D\uDDD1";
    private final String SERIAL_WAS_DELETED = "Сериал удален";
    private final String SERIAL_WAS_ADD = "%s сериал добавлен";
    private final String SERIAL_SHORT_INFORMATION = "%s \nТекущий сезон: %s\nПросмотрено серий: %s\nБольше информации: https://www.themoviedb.org/tv/%s";
    private final String CANCEL = "Отмена операции";
    private final String REPEAT_SEARCH = "Повторить поиск";
    private final String SERIAL_CATEGORIES = "Категории сериалов";
    private final String WATCHED = "✅";

    private static HashMap<String, Serial> serials = new HashMap<>();

    private ExecutorService executorService = Executors.newCachedThreadPool();

    private String botToken = "";
    private String filmsToken = "";

    TelegramBot() {
        super();
        try {
            LineIterator botLt = FileUtils.lineIterator(new File("botToken"));
            LineIterator filmsLt = FileUtils.lineIterator(new File("filmsToken"));
            botToken = botLt.nextLine();
            filmsToken = filmsLt.nextLine();
            System.out.println("Bot started");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private ReplyKeyboardMarkup crateMainMenu() {
        KeyboardButton createSerialButton = new KeyboardButton();
        KeyboardRow createSerialButtonRow = new KeyboardRow();
        createSerialButton.setText(ADD_NEW_SERIAL);
        createSerialButtonRow.add(createSerialButton);

        KeyboardButton listSerialsButton = new KeyboardButton();
        KeyboardRow listSerialsButtonRow = new KeyboardRow();
        listSerialsButton.setText(LIST_SERIALS);
        listSerialsButtonRow.add(listSerialsButton);

        KeyboardButton settingsButton = new KeyboardButton();
        KeyboardRow settingsButtonRow = new KeyboardRow();
        settingsButton.setText(SETTINGS);
        settingsButtonRow.add(settingsButton);

        List<KeyboardRow> keyboardRowList = new ArrayList<>();
        keyboardRowList.add(createSerialButtonRow);
        keyboardRowList.add(listSerialsButtonRow);
        keyboardRowList.add(settingsButtonRow);

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setKeyboard(keyboardRowList);
        replyKeyboardMarkup.setOneTimeKeyboard(true);
        return replyKeyboardMarkup;
    }

    private void sendFinishedMessage(SendMessage message) {
        try {
            System.out.println(String.format("Send | %s", message));
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void editFinishedMessage(EditMessageText message) {
        try {
            System.out.println(String.format("Edit | %s", message));
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void deleteFinishedMessage(DeleteMessage message) {
        try {
            System.out.println(String.format("Delete | %s", message));
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void openMainMenuMessage(Message message) {
        SendMessage sendMessage = new SendMessage()
                .setChatId(message.getChatId())
                .setText(MAIN_MENU)
                .setReplyMarkup(crateMainMenu());
        sendFinishedMessage(sendMessage);
    }

    private void openMainMenuFromCallback(Message message) {
        EditMessageText editMessageText = new EditMessageText()
                .setChatId(message.getChatId())
                .setMessageId(message.getMessageId())
                .setText(MAIN);
        SendMessage sendMessage = new SendMessage()
                .setReplyMarkup(crateMainMenu())
                .setChatId(message.getChatId())
                .setText(MENU);
        editFinishedMessage(editMessageText);
        sendFinishedMessage(sendMessage);
    }

    private void startUser(Message message) {
        User user = searchUser(database, message.getChatId());
        if (user == null) {
            User newUser = new User(message.getChatId());
            database.child("users").child(newUser.getChatId().toString()).setValue(newUser, null);
        }
        openMainMenuMessage(message);
    }

    private void startAddNewSerialInline(Callback callback, Message message) {
        User user = searchUser(database, message.getChatId());
        if (user == null) {
            sendNoFoundUserError(message.getChatId());
        } else {
            user.setStep(User.STEP_WAIT_NAME_SERIAL);
            database.child("users").child(user.getChatId().toString()).setValue(user, null);
            EditMessageText editMessageText = new EditMessageText()
                    .setText(message.isGroupMessage()
                            ? GETTING_SEARCH_GROUP
                            : GETTING_SEARCH)
                    .setMessageId(message.getMessageId())
                    .setChatId(message.getChatId());
            editFinishedMessage(editMessageText);
        }
    }

    private void startAddNewSerial(Message message) {
        User user = searchUser(database, message.getChatId());
        if (user == null) {
            sendNoFoundUserError(message.getChatId());
        } else {
            user.setStep(User.STEP_WAIT_NAME_SERIAL);
            database.child("users").child(user.getChatId().toString()).setValue(user, null);
            SendMessage sendMessage = new SendMessage()
                    .setChatId(user.getChatId())
                    .setText(message.isGroupMessage()
                            ? GETTING_SEARCH_GROUP
                            : GETTING_SEARCH);
            sendFinishedMessage(sendMessage);
        }
    }

    private void sendResultSearchSerials(User user, Message message) {
        List<Serial> serialsFound = searchSerials(serials, filmsToken, message.getText());
        ArrayList<ArrayList<InlineButtonInformation>> lists = new ArrayList<>();
        lists.add(new ArrayList<>());
        StringBuilder messageText = new StringBuilder(serialsFound.size() > 0 ? RESULTS : NOT_FOUND_SEARCH);
        if (serialsFound.size() > 0) {
            user.setStep(User.STEP_START);
            database.child("users").child(user.getChatId().toString()).setValue(user, null);
        }
        int counter = 1;
        for (Serial serial : serialsFound) {
            if (serial.date == null) {
                messageText.append(String.format("*%s. %s*\n`%s`\n\n", counter, serial.title, serial.description.replace("\n\n", "\n")));
            } else {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(serial.date);
                String description = serial.description.replace("\n\n", "\n");
                messageText.append(String.format("*%s. %s (%s)*\n`%s`\nhttps://www.themoviedb.org/tv/%s\n\n", counter, serial.title, calendar.get(Calendar.YEAR),
                        description.substring(0, description.length() > 800 ? 800 : description.length()), serial.getId()));
            }
            lists.get(0).add(new InlineButtonInformation(String.format("%s.", counter), serial.getId(), Callback.TYPE_SELECT_NEW_SERIAL, null, null));
            counter++;
        }

        lists.add(new ArrayList<>(Collections.singletonList(new InlineButtonInformation(MAIN_MENU, null, Callback.TYPE_MAIN_MENU, null, null))));
        lists.add(new ArrayList<>(Collections.singletonList(new InlineButtonInformation(REPEAT_SEARCH, null, Callback.TYPE_ADD_NEW_SERIAL_INLINE, null, null))));

        SendMessage sendMessage = new SendMessage()
                .setText(messageText.toString())
                .setChatId(message.getChatId())
                .setReplyMarkup(Utils.inlineKeyboardBuilder(lists))
                .disableWebPagePreview()
                .setParseMode("Markdown");
        sendFinishedMessage(sendMessage);
    }

    private void sendNoFoundUserError(Long chatId) {
        SendMessage sendMessage = new SendMessage()
                .setText(USER_NOT_FOUND)
                .setChatId(chatId);
        sendFinishedMessage(sendMessage);
    }

    private void showListSerials(final Message message, Integer categroie) {
        User user = searchUser(database, message.getChatId());
        if (user == null) {
            sendNoFoundUserError(message.getChatId());
        } else {
            ArrayList<ArrayList<InlineButtonInformation>> lists = new ArrayList<>();
            user.getMediaInformations().forEach(media -> {
                if (media.getStatus().equals(categroie)) {
                    ArrayList<InlineButtonInformation> list = new ArrayList<>();
                    list.add(new InlineButtonInformation(searchSerial(serials, filmsToken, media.getId()).getTitle(),
                            media.getId(), Callback.TYPE_SELECT_SERIAL, null, null));
                    lists.add(list);
                }
            });
            lists.add(new ArrayList<>(Collections.singletonList(new InlineButtonInformation(MAIN_MENU, null, Callback.TYPE_MAIN_MENU, null, null))));
            String messageText = lists.size() > 1 ? LIST_ACTIVE_SERIALS : NO_ACTIVE_SERIALS;
            EditMessageText editMessageText = new EditMessageText()
                    .setMessageId(message.getMessageId())
                    .setChatId(user.getChatId())
                    .setText(messageText)
                    .setReplyMarkup(Utils.inlineKeyboardBuilder(lists));
            user.setStep(User.STEP_START);
            database.child("users").child(user.getChatId().toString()).setValue(user, null);
            editFinishedMessage(editMessageText);
        }
    }

    private void showSerialCategories(Message message) {
        User user = searchUser(database, message.getChatId());
        if (user == null) {
            sendNoFoundUserError(message.getChatId());
        } else {
            ArrayList<ArrayList<InlineButtonInformation>> lists = new ArrayList<>();
            HashMap<String, Integer> categroies = new HashMap<>();
            categroies.put("Запланированно", MediaInformation.STATUS_PLANNED);
            categroies.put("Смотрю", MediaInformation.STATUS_WATCH);
            categroies.put("Отложенно", MediaInformation.STATUS_PENDING);
            categroies.put("Просмотренно", MediaInformation.STATUS_VIEWED);
            categroies.forEach((text, number) -> lists.add(new ArrayList<>(Collections.singletonList(
                    new InlineButtonInformation(text, null, Callback.TYPE_SHOW_SERIAL_CATEGORIES, null, number.toString())))));
            lists.add(new ArrayList<>(Collections.singletonList(new InlineButtonInformation(MAIN_MENU, null, Callback.TYPE_MAIN_MENU, null, null))));
            SendMessage sendMessage = new SendMessage()
                    .setChatId(user.getChatId())
                    .setText(SERIAL_CATEGORIES)
                    .setReplyMarkup(Utils.inlineKeyboardBuilder(lists));
            sendFinishedMessage(sendMessage);
        }
    }

    private InlineKeyboardMarkup serialInlineKeyboard(Serial serial, User user) {
        ArrayList<ArrayList<InlineButtonInformation>> lists = new ArrayList<>();
        IntStream.range(0, 3).forEach(i -> lists.add(new ArrayList<>()));
        String[] changeValues = {"-3", "-2", "-1", "+1", "+2", "+3"};
        for (String value : changeValues) {
            Integer newCount = user.searchMediaInformation(serial.getId()).getCount() + Integer.parseInt(value);
            if (newCount <= serial.episodesForSeasonFromStart(serial.getEpisodes().size()) && newCount >= 0) {
                lists.get(0).add(new InlineButtonInformation(value, serial.getId(), Callback.TYPE_ACTION, Callback.ACTION_CHANGE_COUNT, value));
            }
        }
        if (serial.currentSeason(user.searchMediaInformation(serial.getId()).getCount()) > 0) {
            lists.get(1).add(new InlineButtonInformation(PREVIOUS_SEASON, serial.getId(), Callback.TYPE_ACTION, Callback.ACTION_PREVIOUS_SEASON, null));
        }
        if (serial.currentSeason(user.searchMediaInformation(serial.getId()).getCount()) < serial.getEpisodes().size()) {
            lists.get(1).add(new InlineButtonInformation(NEXT_SEASON, serial.getId(), Callback.TYPE_ACTION, Callback.ACTION_NEXT_SEASON, null));
        }
        if (!user.searchMediaInformation(serial.getId()).getStatus().equals(MediaInformation.STATUS_VIEWED)) {
            lists.get(1).add(new InlineButtonInformation(WATCHED, serial.getId(), Callback.TYPE_ACTION, Callback.ACTION_CHECK_WATCHED, null));
        }
        lists.get(2).add(new InlineButtonInformation(DELETE_SERIAL, serial.getId(), Callback.TYPE_ACTION, Callback.ACTION_DELETE, null));
        lists.get(2).add(new InlineButtonInformation(MAIN_MENU, serial.getId(), Callback.TYPE_MAIN_MENU, null, null));
        return Utils.inlineKeyboardBuilder(lists);
    }

    private void showSerial(CallbackQuery callbackQuery) {
        Callback callback = new Callback(callbackQuery.getData());
        User user = searchUser(database, callbackQuery.getMessage().getChatId());
        if (user == null) {
            sendNoFoundUserError(callbackQuery.getMessage().getChatId());
        } else {
            Serial serial = searchSerial(serials, filmsToken, callback.getId());
            Integer currentSeason = serial.currentSeason(user.searchMediaInformation(serial.getId()).getCount());
            EditMessageText editMessageText = new EditMessageText()
                    .setChatId(user.getChatId())
                    .setMessageId(callbackQuery.getMessage().getMessageId())
                    .disableWebPagePreview()
                    .setText(String.format(SERIAL_SHORT_INFORMATION, serial.getTitle(),
                            currentSeason + 1, user.searchMediaInformation(serial.getId()).getCount() - serial.episodesForSeasonFromStart(currentSeason),
                            serial.getId()))
                    .setReplyMarkup(serialInlineKeyboard(serial, user));
            editFinishedMessage(editMessageText);
        }
    }

    private void changeCountWatchedSerial(User user, Integer count, String callbackSerialId, Serial serial, CallbackQuery callback) {
        MediaInformation information = user.searchMediaInformation(serial.getId());
        information.setCount(count);
        if (count.equals(serial.episodesForSeasonFromStart(serial.getEpisodes().size()))) {
            information.setStatus(MediaInformation.STATUS_VIEWED);
        } else {
            information.setStatus(MediaInformation.STATUS_WATCH);
        }
        database.child("users").child(user.getChatId().toString()).setValue(user, null);
        if (callbackSerialId.equals(serial.getId())) {
            Integer currentSeason = serial.currentSeason(user.searchMediaInformation(serial.getId()).getCount());
            EditMessageText editMessageText = new EditMessageText()
                    .setText(String.format(SERIAL_SHORT_INFORMATION, serial.getTitle(),
                            currentSeason + 1, user.searchMediaInformation(serial.getId()).getCount() - serial.episodesForSeasonFromStart(currentSeason),
                            serial.getId()))
                    .disableWebPagePreview()
                    .setMessageId(callback.getMessage().getMessageId())
                    .setChatId(callback.getMessage().getChatId())
                    .setReplyMarkup(serialInlineKeyboard(serial, user));
            editFinishedMessage(editMessageText);
        }
    }

    private void actionWithSerial(User user, CallbackQuery callbackQuery) {
        Callback callback = new Callback(callbackQuery.getData());
        Serial serial = searchSerial(serials, filmsToken, callback.getId());
        Integer count = user.searchMediaInformation(serial.getId()).getCount();
        switch (callback.getAction()) {
            case Callback.ACTION_CHANGE_COUNT:
                changeCountWatchedSerial(user, count + Integer.parseInt(callback.getData()),
                        callback.getId(), serial, callbackQuery);
                break;
            case Callback.ACTION_DELETE:
                user.getMediaInformations().removeIf(media -> media.getId().equals(callback.getId()));
                database.child("users").child(user.getChatId().toString()).setValue(user, null);
                EditMessageText editMessageText = new EditMessageText()
                        .setText(SERIAL_WAS_DELETED)
                        .setMessageId(callbackQuery.getMessage().getMessageId())
                        .setChatId(callbackQuery.getMessage().getChatId());
                editFinishedMessage(editMessageText);
                startUser(callbackQuery.getMessage());
                break;
            case Callback.ACTION_NEXT_SEASON:
                changeCountWatchedSerial(user,
                        serial.episodesForSeasonFromStart(serial.currentSeason(count) + 1),
                        callback.getId(), serial, callbackQuery);
                break;
            case Callback.ACTION_PREVIOUS_SEASON:
                changeCountWatchedSerial(user,
                        serial.episodesForSeasonFromStart(serial.currentSeason(count) - 1),
                        callback.getId(), serial, callbackQuery);
                break;
            case Callback.ACTION_CHECK_WATCHED:
                changeCountWatchedSerial(user, serial.episodesForSeasonFromStart(serial.getEpisodes().size()),
                        callback.getId(), serial, callbackQuery);
                break;
        }
    }


    private void addNewSerial(User user, CallbackQuery callbackQuery) {
        Callback callback = new Callback(callbackQuery.getData());
        Serial serial = searchSerial(serials, filmsToken, callback.getId());
        user.getMediaInformations().add(new MediaInformation(serial.getId(), MediaInformation.TYPE_MOVIE));
        user.setStep(User.STEP_START);
        database.child("users").child(user.getChatId().toString()).setValue(user, null);
        ArrayList<ArrayList<InlineButtonInformation>> lists = new ArrayList<>();
        lists.add(new ArrayList<>(Collections.singletonList(new InlineButtonInformation(ADD_MORE_SERIAL, null, Callback.TYPE_ADD_NEW_SERIAL_INLINE, null, null))));
        lists.add(new ArrayList<>(Collections.singletonList(new InlineButtonInformation(MAIN_MENU, null, Callback.TYPE_MAIN_MENU, null, null))));
        EditMessageText editMessageText = new EditMessageText()
                .setText(String.format(SERIAL_WAS_ADD, serial.getTitle()))
                .setChatId(user.getChatId())
                .setMessageId(callbackQuery.getMessage().getMessageId())
                .setReplyMarkup(Utils.inlineKeyboardBuilder(lists));
        editFinishedMessage(editMessageText);
    }

    private void cancelOperation(Message message) {
        User user = searchUser(database, message.getChatId());
        if (user == null) {
            sendNoFoundUserError(message.getChatId());
        } else {
            user.setStep(User.STEP_START);
            database.child("users").child(user.getChatId().toString()).setValue(user, null);
            SendMessage sendMessage = new SendMessage()
                    .setText(CANCEL)
                    .setChatId(user.getChatId())
                    .setReplyMarkup(crateMainMenu());
            sendFinishedMessage(sendMessage);
        }
    }

    private void checkUserCallback(CallbackQuery callbackQuery) {
        User user = searchUser(database, callbackQuery.getMessage().getChatId());
        Callback callback = new Callback(callbackQuery.getData());
        if (user == null) {
            sendNoFoundUserError(callbackQuery.getMessage().getChatId());
        } else {
            try {
                switch (callback.getType()) {
                    case Callback.TYPE_ACTION:
                        actionWithSerial(user, callbackQuery);
                        break;
                    case Callback.TYPE_SELECT_NEW_SERIAL:
                        addNewSerial(user, callbackQuery);
                        break;
                    case Callback.TYPE_MAIN_MENU:
                        openMainMenuFromCallback(callbackQuery.getMessage());
                        break;
                    case Callback.TYPE_ADD_NEW_SERIAL:
                        startAddNewSerial(callbackQuery.getMessage());
                        break;
                    case Callback.TYPE_ADD_NEW_SERIAL_INLINE:
                        startAddNewSerialInline(callback, callbackQuery.getMessage());
                        break;
                    case Callback.TYPE_SELECT_SERIAL:
                        showSerial(callbackQuery);
                        break;
                    case Callback.TYPE_SHOW_SERIAL_CATEGORIES:
                        showListSerials(callbackQuery.getMessage(), Integer.parseInt(callback.getData()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void checkUserStep(Message message) {
        User user = searchUser(database, message.getChatId());
        if (user == null) {
            sendNoFoundUserError(message.getChatId());
        } else {
            if (user.getStep().equals(User.STEP_WAIT_NAME_SERIAL)) {
                sendResultSearchSerials(user, message);
            }
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            Message message = update.getMessage();
            if (message.getText().equals("/start") || message.getText().equals("/start@" + getBotUsername())) {
                executorService.submit(() -> startUser(update.getMessage()));
            } else if (message.getText().equals("/cancel") || message.getText().equals("/cancel@" + getBotUsername())) {
                executorService.submit(() -> cancelOperation(message));
            } else if (message.getText().equals(ADD_NEW_SERIAL)) {
                executorService.submit(() -> startAddNewSerial(message));
            } else if (message.getText().equals(LIST_SERIALS)) {
//                executorService.submit(() -> showListSerials(message));
                executorService.submit(() -> showSerialCategories(message));
            } else {
                executorService.submit(() -> checkUserStep(message));
            }
        } else if (update.hasCallbackQuery()) {
            executorService.submit(() -> checkUserCallback(update.getCallbackQuery()));
        }
    }

    @Override
    public String getBotUsername() {
        return "TiiRiiXBot";
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}
