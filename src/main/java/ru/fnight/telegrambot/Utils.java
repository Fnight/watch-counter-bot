package ru.fnight.telegrambot;

import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.ArrayList;

class Utils {
    static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder response = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    static InlineKeyboardMarkup inlineKeyboardBuilder(ArrayList<ArrayList<InlineButtonInformation>> lists) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> listsButton = new ArrayList<>();
        for (ArrayList<InlineButtonInformation> list : lists) {
            List<InlineKeyboardButton> buttons = new ArrayList<>();
            for (InlineButtonInformation info : list) {
                InlineKeyboardButton button = new InlineKeyboardButton();
                button.setText(info.getText());
                Callback callback = new Callback(info.getId(), info.getType(), info.getAction(), info.getData());
                button.setCallbackData(callback.toJson());
                buttons.add(button);
            }
            listsButton.add(buttons);
        }
        inlineKeyboardMarkup.setKeyboard(listsButton);
        return inlineKeyboardMarkup;
    }


}

class InlineButtonInformation {
    private String text;
    private String id;
    private String type;
    private String action;
    private String data;

    public InlineButtonInformation(String text, String id, String type, String action, String data) {
        this.text = text;
        this.id = id;
        this.type = type;
        this.action = action;
        this.data = data;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
