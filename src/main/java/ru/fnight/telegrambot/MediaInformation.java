package ru.fnight.telegrambot;

public class MediaInformation {
    public static final Integer STATUS_PLANNED = 1;
    public static final Integer STATUS_WATCH = 2;
    public static final Integer STATUS_PENDING = 3;
    public static final Integer STATUS_VIEWED = 4;

    public static final Integer TYPE_SERIAL = 1;
    public static final Integer TYPE_MOVIE = 2;

    private String id;
    private Integer type;
    private Integer count;
    private Integer status;

    public MediaInformation() {
    }

    public MediaInformation(String id, Integer type) {
        this.id = id;
        this.type = type;
        this.count = 0;
        this.status = STATUS_WATCH;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
